package ifrn.tads.corporativo.dao;

import java.util.ArrayList;

import ifrn.tads.corporativo.dominio.Classificado;

public class ClassificadoDAO {
	
	private ArrayList<Classificado> classificados;
	
	public void add(Classificado classificado){
		classificados.add(classificado);
	}
	
	public ArrayList<Classificado> list(){
		return classificados;
	}
}
