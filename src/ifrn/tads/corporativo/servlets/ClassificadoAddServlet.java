package ifrn.tads.corporativo.servlets;

import ifrn.tads.corporativo.dao.ClassificadoDAO;
import ifrn.tads.corporativo.dominio.Classificado;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/classificados/add")
public class ClassificadoAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ClassificadoAddServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/Classificado/add.jsp").forward(request,
				response);
	}
	
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String titulo = request.getParameter("titulo");
		String descricao = request.getParameter("descricao");
		String conteudo = request.getParameter("conteudo");
		
		ClassificadoDAO classificadoDAO = new ClassificadoDAO();
		Classificado classificado = new Classificado(titulo, descricao, conteudo);
		classificadoDAO.add(classificado);

		request.getRequestDispatcher("/Classificado/add.jsp").forward(request,response);
	}
}
