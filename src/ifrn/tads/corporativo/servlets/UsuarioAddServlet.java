package ifrn.tads.corporativo.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/usuarios/add")
public class UsuarioAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
    public UsuarioAddServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/Usuario/add.jsp").forward(request, response);
	}
}
