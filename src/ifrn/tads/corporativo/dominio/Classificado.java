package ifrn.tads.corporativo.dominio;

public class Classificado {
	private String titulo, resumo, conteudo;
	
	public Classificado(String titulo, String resumo, String conteudo) {
		this.titulo = titulo;
		this.resumo = resumo;
		this.conteudo = conteudo;
	}

	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getResumo() {
		return resumo;
	}
	public void setResumo(String resumo) {
		this.resumo = resumo;
	}
	public String getConteudo() {
		return conteudo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}	
}
