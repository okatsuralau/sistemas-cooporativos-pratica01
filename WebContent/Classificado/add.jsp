<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<title>Usu�rio</title>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
		<div class="container">
			<a class="navbar-brand" href="/sistemas-coorporativos-pratica01">Sistemas corporativos - Pratica 01</a>
			<ul class="nav navbar-nav">
				<li><a href="/sistemas-coorporativos-pratica01/usuarios">Usu�rio</a></li>
				<li><a href="/sistemas-coorporativos-pratica01/classificados">Classificado</a></li>
			</ul>
		</div>
	</nav>
	<div class="container">
		<!-- miolo das p�ginas aqui -->
		<h1 class="page-header">Novo classificado</h1>
		<form  method="post">
			<div class="form-group">
				<input type="text" name="titulo" placeholder="Titulo" class="form-control" />
			</div>
			<div class="form-group">
				<input type="text" name="descricao" placeholder="Descri��o" class="form-control" />
			</div>
			<div class="form-group">
				<textarea name="conteudo" placeholder="Conte�do" class="form-control"></textarea>
			</div>
			
			<button type="submit" class="btn btn-primary">Cadastrar</button>
		</form>
	</div>
</body>
</html>